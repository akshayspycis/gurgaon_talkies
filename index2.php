<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<!-- Mirrored from htmlcoder.me/preview/the_project/v.1.0/template/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jul 2015 17:34:54 GMT -->
        <head>
		<meta charset="utf-8">
		<title>Gurgaon Talkies | Home</title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<?php include 'includes/css.php';?>
                <style>

#thirdrow {
    background-image: url(images/digitalbg.jpg);
    background-repeat: no-repeat;
    box-shadow: 0 0 10px rgba(0,0,0,0.8);
    background-position: center center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}    
    


                    #ht {
    background: #4F3922 url(images/bg_subheader.png);
    background-repeat: repeat;
}
                    .brow-light{color: #fff;}
                    .header-gurgaon{
                        background: rgb(0,0,0,0.7);
                        border-top: 1px solid #7b7158;
                        border-bottom: 2px solid #706343;
                    }
                    .nav-li-a {
                            font-family: "Brush Script MT";
                            font-size: 23px;
                            color: #d5ba80;
                    }
                    .background-img-gurgoan{
                        background-image: url(images/line-art-1.png);background-repeat:no-repeat;background-position: 40% 102%;
                    }
                    .icon-img-center-box{
                        margin: auto;
                    }
                    @font-face {
font-family: "Brush Script MT";
src: url(fonts/fontello/font/brushsci.ttf);
}
                    @font-face {
font-family: "Monotype Corsiva";
src: url(fonts/fontello/font/Monotype.ttf);
}
                    .icon-img-title-strong{
                        
                        /*font-family: "Brush Script MT";*/
/*                        font-size: 30px;
                        font-weight: 500;
                        line-height: 26.4px;*/
                        color: #fff;
                    }
                    .h0-Brush-Script-MT{
                        /*font-family: "Brush Script MT";*/
/*                        font-size: 50px;
                        font-weight: 600;
                        line-height: 26.4px;*/
                        color: #d5ba80;
                    }
                    .h1-Brush-Script-MT{
                        /*font-family: "Brush Script MT";*/
/*                        font-size: 40px;
                        font-weight: 500;
                        line-height: 26.4px;*/
                        color: #d5ba80;
                    }
                    .h2-Brush-Script-MT{
                        font-family: "Brush Script MT";
                        font-size: 30px;
                        font-weight: 500;
                        line-height: 26.4px;
                        color: #fff;
                    }
                    .h3-Brush-Script-MT{
                        /*font-family: "Monotype Corsiva";*/
/*                        font-weight: 500;
                        font-size: 20px;
                        line-height: 1.4;*/
                        color: #d5ba80;
                    }
                    .footer-Brush-Script-MT{
                        /*font-family: "Monotype Corsiva";*/
/*                        font-weight: 500;
                        font-size: 20px;
                        line-height: 1.4;*/
                        color: #d5ba80;
                    }
                    .h4-Brush-Script-MT{
                        /*font-family: "Monotype Corsiva";*/
/*                        font-weight: 500;
                        font-size: 20px;
                        line-height: 1.4;*/
                        color: #d5ba80;
                    }
                    .icon-title-Brush-Script-MT {
                            /*text-align: center;*/
                            /*font-family: "Brush Script MT";*/
                            /*font-size: 20px;*/
                            /*font-weight: 600;*/
                            /*line-height: 26.4px;*/
                        color: #d5ba80;
                    }
                </style>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header " >
		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
                <div class="page-wrapper background-body">
                    <?php include 'includes/header.php'; ?>
                    <div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
				<div class="slideshow">
					
					<!-- slider revolution start -->
					<!-- ================ -->
					<div class="slider-banner-container">
						<div class="slider-banner-fullscreen">
							<ul class="slides">
                                                            <li data-transition="random-static" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Fine Dining">
								
								<!-- main image -->
                                                                <img src="images/banner-3.png" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
								
								<!-- Transparent Background -->
								<div class="tp-caption"
									data-x="center"
									data-y="bottom"
									data-speed="500"
									data-easing=""
									data-start="0">
								</div>
								<!-- LAYER NR. 4 -->
								

								<!-- LAYER NR. 5 -->
								

								<!-- LAYER NR. 6 -->
								

								<!-- LAYER NR. 7 -->
								


								</li>
								<li data-transition="random-static" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Exquisite Bar">
								
								<!-- main image -->
                                                                <img src="images/banner-6.jpg" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
								
								<!-- Transparent Background -->
								<div class="tp-caption"
									data-x="center"
									data-y="bottom"
									data-speed="500"
									data-easing=""
									data-start="0">
								</div>
								<!-- LAYER NR. 4 -->
								

								<!-- LAYER NR. 5 -->
								

								<!-- LAYER NR. 6 -->
								

								<!-- LAYER NR. 7 -->
								


								</li>
								<li data-transition="random-static" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Rain Dancing">
								
								<!-- main image -->
                                                                <img src="images/banner-3.png" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
								
								<!-- Transparent Background -->
								<div class="tp-caption"
									data-x="center"
									data-y="bottom"
									data-speed="500"
									data-easing=""
									data-start="0">
								</div>
								<!-- LAYER NR. 4 -->
								

								<!-- LAYER NR. 5 -->
								

								<!-- LAYER NR. 6 -->
								

								<!-- LAYER NR. 7 -->
								


								</li>
							</ul>
							<div class="tp-bannertimer"></div>
						</div>
					</div>
					<!-- slider revolution end -->

				</div>
				<!-- slideshow end -->

			</div>
                        <section class="light-gray-bg pv-30 clearfix" style="background: url(http://3415_vapourbarexchange.limetray.com/assets/clt/3415_vapourbarexchange.limetray.com/midimg00.jpg) fixed;">
                        <div class="container" style="background: rgb(0,0,0,0.7)">
                                    <center>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                    <p class="h2-Brush-Script-MT" style="font-size: 30px;line-height: 13px;margin-left:-300px; ">Agar Yeh Nahin </p>
                                    <p class="h2-Brush-Script-MT" style="font-size: 40px;line-height: 34px;margin-left: -110px;">Dekha </p>
                                    <p class="h2-Brush-Script-MT" style="font-size: 30px;line-height: 19px;margin-left: 100px;">to Kuch Nahin</p>
                                    <p class="h2-Brush-Script-MT" style="font-size: 40px;line-height: 20px;margin-left: 324px;">Dekha</p>
                                    <div class="separator-3 visible-lg"></div>
                                    <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                    </center>
				</div>
			</section>
                    <section class="pv-40 stats padding-bottom-clear dark-translucent-bg hovered "  
                             style="background: url(images/banner.jpg) fixed;">
				<div class="clearfix" background: rgb(0,0,0,0.7);background-position: 50% 50%;border-bottom: 1px solid #f3f3f3;">
					<div class="col-md-2 col-xs-6 text-center" id="layer_trans_icon">
						<div class="feature-box object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="300">
                                                    <img class="icon-img-center-box" src="images/iocn-1.png"/ >
                                                    <h4><strong class="icon-img-title-strong">Drive In Theatre</strong></h4>
						</div>
					</div>
					<div class="col-md-2 col-xs-6 text-center" id="layer_trans_icon">
						<div class="feature-box object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="300">
                                                    <img class="icon-img-center-box" src="images/iocn-2.png"/>
                                                    <h4><strong class="icon-img-title-strong">Rain Dancing</strong></h4>
						</div>
					</div>
					<div class="col-md-2 col-xs-6 text-center" id="layer_trans_icon">
						<div class="feature-box object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="300">
                                                    <img class="icon-img-center-box" src="images/iocn-3.png"/>
                                                    <h4><strong class="icon-img-title-strong">Fun Gaming</strong></h4>
						</div>
					</div>
					<div class="col-md-2 col-xs-6 text-center" id="layer_trans_icon">
						<div class="feature-box object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="300">
                                                    <img class="icon-img-center-box" src="images/iocn-4.png"/>
                                                    
                                                    <h4><strong class="icon-img-title-strong">Relaxing Pool</strong></h4>
						</div>
					</div>
					<div class="col-md-2 col-xs-6 text-center" id="layer_trans_icon">
						<div class="feature-box object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="300">
                                                    <img class="icon-img-center-box" src="images/iocn-5.png"/>
                                                    <h4><strong class="icon-img-title-strong">Fine Dining</strong></h4>
						</div>
					</div>
					<div class="col-md-2 col-xs-6 text-center" id="layer_trans_icon">
						<div class="feature-box object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="300">
                                                    <img class="icon-img-center-box" src="images/iocn-6.png"/>
                                                    <h4><strong class="icon-img-title-strong">Exquisite Bar</strong></h4>
						</div>
					</div>
				</div>
                            </section>
                    <section class="full-width-section">
				<div class=" default-bg">
                                    <img class="to-right-block" src="images/banner-2.png" alt="">
				</div>
                        <div class="full-text-container default-bg" id="thirdrow">
                            <div class="fulltransparency ">
					<h2 class="logo-font">Drive in Theater</h2>
					<div class="separator-2 visible-lg"></div>
					<p>The experience of watching an IPL or movie, seeing the epic Bollywood dialogues sitting inside your car or on top of it, while snacking is the experience you should never miss in your life..</p>
					<div class="separator-3 visible-lg"></div>
                            </div>
			</div>
                    </section>
                    <section class="full-width-section">
                        <div class="full-text-container default-bg" id="thirdrow">
                            <div class="fulltransparency ">
					<h2 class="logo-font">Relaxing Pool</h2>
					<div class="separator-2 visible-lg"></div>
					<p>The beautiful pool is the perfect place for you and your family to relax and enjoy. While the children play, you can calm yourself and enjoy the pool.</p>
					<div class="separator-3 visible-lg"></div>
                            </div>
			</div>
				<div class=" default-bg">
                                    <img class="to-right-block" src="images/relaxing-pool.jpg" alt="">
				</div>
			</section>
                    <section class="full-width-section">
				<div class=" default-bg">
                                    <img class="to-right-block" src="images/banner-1.png" alt="">
				</div>
                        <div class="full-text-container default-bg" id="thirdrow">
                            <div class="fulltransparency ">
					<h2 class="logo-font">Rain Dancing</h2>
					<div class="separator-2 visible-lg"></div>
					<p>You don’t have to wait for the rain in Gurgaon. Rain comes to you whenever you want here in Gurgaon Talkies. Bing your buddies to play with the rain and enjoy.</p>
					<div class="separator-3 visible-lg"></div>
                            </div>
			</div>
                    </section>
                    <section class="full-width-section">
                        <div class="full-text-container default-bg" id="thirdrow">
                            <div class="fulltransparency ">
					<h2 class="logo-font">Fun Gaming</h2>
					<div class="separator-2 visible-lg"></div>
					<p>Many entertaining games for all the age group is available here. Bring in your family and everyone shall be entertained for sure, while you just chill and relax.</p>
					<div class="separator-3 visible-lg"></div>
                            </div>
			</div>
				<div class=" default-bg">
                                    <img class="to-right-block" src="images/fun-gaming.jpg" alt="">
				</div>
			</section>
                    <section class="full-width-section">
				<div class=" default-bg">
                                    <img class="to-right-block" src="images/banner-3.png" alt="">
				</div>
                        <div class="full-text-container default-bg" id="thirdrow">
                            <div class="fulltransparency ">
					<h2 class="logo-font">Fine Dining</h2>
					<div class="separator-2 visible-lg"></div>
					<p>Special party spaces are available on request. Different dine areas are available for multi experiences. Just come and enjoy the food.</p>
					<div class="separator-3 visible-lg"></div>
                            </div>
			</div>
                    </section>
                    <section class="full-width-section">
                        <div class="full-text-container default-bg" id="thirdrow">
                            <div class="fulltransparency ">
					<h2 class="logo-font">Exquisite Bar</h2>
					<div class="separator-2 visible-lg"></div>
					<p>We have exquisite bar with a perfect ambience to enjoy your drink, The semi-open space connected to nature, just perfectly provides the smoothing calming feel to anyone who steps in.</p>
					<div class="separator-3 visible-lg"></div>
                            </div>
			</div>
				<div class=" default-bg">
                                    <img class="to-right-block" src="images/banner-4.png" alt="">
				</div>
			</section>
                    <section class="section dark-translucent-bg" style="background-position: 50% 52%;background:url(images/banner-3.png) fixed;">
                            <div class="" style="background: rgb(0,0,0,0.7)">
					<div class="container" >
                                            <div class="row" >
                                                    <div class="col-md-4">
                                                        <center><img src="images/list.png"></center>
                                                        
						</div>
                                                <div class="col-md-8">
                                                        <p>&nbsp;</p>
                                                        <h2 class="logo-font"><strong>Visitor Information</strong></h2>
                                                        <div class="separator-2 visible-lg"></div>
					<p >Gurgaon talkies was started with a sole motive to create a wholesome never-before-ever experience in Gurgaon.</p>
                                                        <p >Be it IPL, Football League, Bollywood Movie or any classic movies, who wouldn't want to grab some popcorn and have the experience of watching big screen in your car.</p>
                                                        <p >So we give you this Amazing Experience.</p>
                                                        <p >NO No You Don’t have to thank us.</p>
                                                        <p >Just pass on the information for more people to enjoy.</p>
                                                        <p >Let Gurgaon Rock and Roll</p>
					<div class="separator-3 visible-lg"></div>
							
							</div>
						</div>
					</div>
				</div>
			</section>
                            <section class="light-gray-bg pv-30 clearfix" style="background: url(http://3415_vapourbarexchange.limetray.com/assets/clt/3415_vapourbarexchange.limetray.com/midimg00.jpg) fixed;">
                        
                            <div class="space-bottom"style="background: rgb(0,0,0,0.7);">
                                <div class="owl-carousel content-slider owl-theme" id="SelTestimonials" style="opacity: 1; display: block;">
                               <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 5996px; left: 0px; display: block; transition: all 800ms ease; transform: translate3d(-1499px, 0px, 0px);"><div class="owl-item" style="width: 1499px;"><div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="testimonial text-center">
                                                    <div class="testimonial-image avatar-image">
                                                        <img src="images/avatar.jpg" style="height:70px;width:70px;" alt="" title="" class="img-circle ">
                                                    </div>
                                                    <div class="testimonial-body" style="color: #fff;">
                                                        <p class="text-center">Bar & Rain dance floor with DJ. Good ambience. Obviously a noisy place..</p>
                                                        <div class="text-center" >Akshay Bilani</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div></div><div class="owl-item" style="width: 1499px;"><div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="testimonial text-center">
                                                    <div class="testimonial-image avatar-image">
                                                        <img src="images/avatar.jpg" style="height:70px;width:70px;" alt="" title="" class="img-circle">
                                                    </div>
                                                        
                                                    <div class="testimonial-body" style="color: #fff;">
                                                            <p class="text-center">Probably the best place I visited in Dehli. The open pool aethetics is incredible and the DJ played a good mix. The cost of food and drinks are on the lower side though. But its a great place to enjoy a weekend..</p>
                                                            <div class="text-center">Manish Trivedi</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div></div></div></div>
                                                                   
                                                                 
                                </div>
                                    
                            </div>
                        </section>
                            <section class="pv-30 " style="background-position: 50% 52%;background:url(images/banner-4.png) fixed;">
                            <div class="" >
					<div class="container">
					<div class="row">
                                            
						<div class="col-md-8 col-md-offset-2" style="color: #fff">
                                                    <p>&nbsp;</p>
                                                    <h2 class="logo-font text-center"><strong style="color: #fff">Our Gallery</strong></h2>
							<div class="separator"></div>
							<p class="large text-center ">All the magic at one place.Movie and Food lovers, we bring all together.Cinema with big screen, Awesome Aquatic, Amazing Foods, Pool n Rain Parties</p>
							<br>
						</div>
					</div>
                                            
                                        </div>
                                <div class="owl-carousel carousel">
						<div class="image-box shadow text-center">
							<div class="overlay-container">
                                                            <img src="images/gallery/2 (14).jpeg" alt="">
								<div class="overlay-top">
									<div class="text">
                                                                            <h3><a href="portfolio-item.html" class="h2-Brush-Script-MT" id="brow-light">Drive In Theatre</a></h3>
                                                                            <p class="small h3-Brush-Script-MT" id="brow-light">The experience of watching.</p>
									</div>
								</div>
								<div class="overlay-bottom" >
									<div class="links">
										<a href="#" id="brow-light" class="btn btn-gray-transparent btn-animated h3-Brush-Script-MT">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container">
                                                            <img src="images/gallery/2 (12).jpeg" alt="">
								<div class="overlay-top">
									<div class="text">
                                                                            <h3><a href="#" class="h2-Brush-Script-MT" id="brow-light">Rain Dancing</a></h3>
                                                                            <p class="small h3-Brush-Script-MT" id="brow-light">You don’t have to wait for the rain.</p>
									</div>
								</div>
								<div class="overlay-bottom" >
									<div class="links">
										<a href="#" id="brow-light" class="btn btn-gray-transparent btn-animated h3-Brush-Script-MT">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container">
                                                            <img src="images/gallery/2 (15).jpeg" alt="">
								<div class="overlay-top">
									<div class="text">
                                                                            <h3><a href="#" class="h2-Brush-Script-MT" id="brow-light">Fun Gaming</a></h3>
                                                                            <p class="small h3-Brush-Script-MT" id="brow-light">Many entertaining games for all the age.</p>
									</div>
								</div>
								<div class="overlay-bottom" >
									<div class="links">
										<a href="#" id="brow-light" class="btn btn-gray-transparent btn-animated h3-Brush-Script-MT">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container">
                                                            <img src="images/gallery/2 (17).jpeg" alt="">
								<div class="overlay-top">
									<div class="text">
                                                                            <h3><a href="#" class="h2-Brush-Script-MT" id="brow-light">Fine Dining</a></h3>
                                                                            <p class="small h3-Brush-Script-MT" id="brow-light">Special party spaces are available on request.</p>
									</div>
								</div>
								<div class="overlay-bottom" >
									<div class="links">
										<a href="#" id="brow-light" class="btn btn-gray-transparent btn-animated h3-Brush-Script-MT">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container">
                                                            <img src="images/gallery/2 (17).jpeg" alt="">
								<div class="overlay-top">
									<div class="text">
                                                                            <h3><a href="#" class="h2-Brush-Script-MT" id="brow-light">Exquisite Bar</a></h3>
                                                                            <p class="small h3-Brush-Script-MT" id="brow-light">We have exquisite bar with a perfect ambience to enjoy your drink.</p>
									</div>
								</div>
								<div class="overlay-bottom" >
									<div class="links">
										<a href="#" id="brow-light" class="btn btn-gray-transparent btn-animated h3-Brush-Script-MT">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
                                
			</section>
                        <?php include 'includes/footer.php';?>
			
			<!-- footer end -->
			
		</div>
                
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<?php include 'includes/jslink.php';?>
	</body>
        <script>
            $(window).resize(function (){
//                document.location="index.php";
            })
            $(document).ready(function (){
               if($(window).width()<892){
                   setReposive();
               }
            });
            function setReposive(){
                $("#logo_img").css({'margin-top':'-5px'});
                $("#layer_trans_icon").css({'padding-left':'0px','padding-right':'0px'});
                $("#all_gallery").trigger("click");
                $("#cricket_icon").css({'margin-left':'20px'});
                $(".header-top").css({'display':'none'});
            }
        </script>
</html>
