<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<!-- Mirrored from htmlcoder.me/preview/the_project/v.1.0/template/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jul 2015 17:34:54 GMT -->
        <head>
		<meta charset="utf-8">
		<title>Gurgaon Talkies | Home</title>
		<meta name="description" content="All the magic at one place.Movie and Food lovers, we bring all together.Cinema with big screen, Awesome Aquatic, Amazing Foods, Pool n Rain Parties">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<?php include 'includes/css.php';?>
                
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner header-top clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
				<div class="slideshow">
					
					<!-- slider revolution start -->
					<!-- ================ -->
					<div class="slider-banner-container">
						<div class="slider-banner-fullwidth-big-height">
							<ul class="slides">

								
								<li data-transition="slidehorizontal" data-slotamount="1"  data-masterspeed="1000" data-fstransition="fade" data-fsmasterspeed="1000" data-saveperformance="off" data-title="The Restaurant">
								
								<!-- main image -->
								<img src="videos/restaurant-video-banner-poster.jpg" alt="slidebg1" data-bgposition="center center"  data-bgrepeat="no-repeat" data-bgfit="cover">
								
								<!-- Transparent Background -->
								<div class="tp-caption dark-translucent-bg"
									data-x="center"
									data-y="bottom"
									data-speed="600"
									data-easing="easeOutQuad"
									data-start="0">
								</div>

								<!-- Video Background -->
								<div class="tp-caption tp-fade fadeout fullscreenvideo"
									data-x="0"
									data-y="0"
									data-speed="1000"
									data-start="1100"
									data-easing="Power4.easeOut"
									data-elementdelay="0.01"
									data-endelementdelay="0.1"
									data-endspeed="1500"
									data-endeasing="Power4.easeIn"
									data-autoplay="true"
									data-autoplayonlyfirsttime="false"
									data-nextslideatend="true"
						 			data-volume="mute" 
						 			data-forceCover="1" 
						 			data-aspectratio="16:9" 
						 			data-forcerewind="on" 
						 			style="z-index: 2;">
									<video class="" preload="none" width="100%" height="100%" poster='videos/restaurant-video-banner-poster.jpg'> 
										<source src='videos/restaurant-background-video-banner.mp4' type='video/mp4'/>
										<source src='videos/restaurant-background-video-banner.webm' type='video/webm'/>
									</video>
								</div>

								<!-- LAYER NR. 1 -->
								<div class="tp-caption sfb fadeout large_white text-center"
									data-x="center"
									data-y="200"
									data-speed="500"
									data-start="1000"
                                                                        data-easing="easeOutQuad"><span class="logo-font"><a href="index.html"><img src="images/logo_light_blue.png" alt="The Project" style="height: 200px;"></a></span>
								</div>	

								<!-- LAYER NR. 4 -->
								<div class="tp-caption fade fadeout"
									data-x="center"
									data-y="bottom"
									data-voffset="-100"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="2000"
									data-endspeed="200">
									<a href="#first_panel" class="btn btn-lg moving smooth-scroll"><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i></a>
								</div>

								</li>
								<!-- slide 2 end -->

							</ul>
							<div class="tp-bannertimer"></div>
						</div>
					</div>
					<!-- slider revolution end -->

				</div>
				<!-- slideshow end -->

			</div>
			<!-- banner end -->

			<!-- header-container start -->
			<!-- ================ -->
			<?php include 'includes/header.php';?>
			<!-- header-container end -->

			<!-- section start -->
			<!-- ================ -->
                        <div class=" banner light-gray-bg">
				<div class=" clearfix">
					<div class="slideshow">
						<div class="slider-banner-container">
							<div class="slider-banner-fullwidth">
								<ul class="slides">
									<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="500" data-saveperformance="on" data-title="">
                                                                            <img src="images/gt-1.jpg" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="">
                                                                            <div class="tp-caption dark-translucent-bg"
                                                                                    data-x="center"
                                                                                    data-y="bottom"
                                                                                    data-speed="600"
                                                                                    data-start="0">
                                                                            </div>
									</li>
									<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="500" data-saveperformance="on" data-title="">
                                                                            <img src="images/gt-2.jpg" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="">
                                                                            <div class="tp-caption dark-translucent-bg"
                                                                                    data-x="center"
                                                                                    data-y="bottom"
                                                                                    data-speed="600"
                                                                                    data-start="0">
                                                                            </div>
									</li>
									<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="500" data-saveperformance="on" data-title="">
                                                                            <img src="images/gt-3.jpg" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="">
                                                                            <div class="tp-caption dark-translucent-bg"
                                                                                    data-x="center"
                                                                                    data-y="bottom"
                                                                                    data-speed="600"
                                                                                    data-start="0">
                                                                            </div>
									</li>
								</ul>
								<div class="tp-bannertimer"></div>
							</div>
						</div>
						<!-- slider revolution end -->

					</div>
					<!-- slideshow end -->

				</div>
			</div>

			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        <section id="dishes" class="section default-bg clearfix" style=" background-color: #42275b;  "></section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			<section class="section clearfix ">
				<div class="container">
<!--					<h2 class="logo-font text-center text-muted">Event</h2>
					<div class="separator"></div>-->
					<section class="full-width-section">
                                    
                        
                        
                        <div class="col-md-6" >
                            <div class="plan shadow light-gray-bg bordered">
                                    <div class="fulltransparency ">
                                        <h2 class="logo-font text-muted text-center">Fine Dining</h2>
                                        <div style="padding:12px;">
                                            <img class="to-right-block" src="images/ev-1.jpg" >
                                        </div>
                                    </div>
                            </div>	
			</div>
                        <div class="col-md-6" >
                            <div class="plan shadow light-gray-bg bordered">
                                    <div class="fulltransparency ">
                                        <h2 class="logo-font text-muted text-center">Rain Dancing</h2>
                                        <div style="padding:12px;">
                                            <img class="to-right-block" src="images/ev-2.jpg" >
                                        </div>
                                    </div>
                            </div>	
			</div>
                        <div class="col-md-6" >
                            <div class="plan shadow light-gray-bg bordered" style="background-color:#eeeeee; ">
                                    <div class="fulltransparency ">
                                        <h2 class="logo-font text-muted text-center">Exquisite Bar</h2>
                                        <div style="padding:12px;">
                                            <img class="to-right-block" src="images/ev-3.jpg" >
                                        </div>
                                    </div>
                            </div>	
			</div>
                        <div class="col-md-6" >
                            <div class="plan shadow light-gray-bg bordered" style="background-color:#eeeeee; ">
                                    <div class="fulltransparency ">
                                        <h2 class="logo-font text-muted text-center">Fun Gaming</h2>
                                        <div style="padding:12px;">
                                            <img class="to-right-block" src="images/ev-4.jpg" >
                                        </div>
                                    </div>
                            </div>	
			</div>
                        <div class="col-md-6" >
                            <div class="plan shadow light-gray-bg bordered">
                                    <div class="fulltransparency ">
                                        <h2 class="logo-font text-muted text-center">Relaxing Pool</h2>
                                        <div style="padding:12px;">
                                            <img class="to-right-block" src="images/ev-5.jpg" >
                                        </div>
                                    </div>
                            </div>	
			</div>
                        <div class="col-md-6" >
                        <div class="plan shadow light-gray-bg bordered" style="background-color:#eeeeee; ">
                        <div class="fulltransparency ">
                        <h2 class="logo-font text-muted text-center">Drive in Theater</h2>
                        <div style="padding:12px;">
                        <img class="to-right-block" src="images/ev-6.jpg" >
                        </div>
                        </div>
                        </div>	
                        </div>
                    </section>
					
				</div>				
			</section>
			<!-- section end -->
                        <section class="section default-bg clearfix" style="    background-color: #4c3263;">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action text-center">
								<div class="row">
									<div class="col-sm-12">
										<h2 class="title logo-font ">Agar Yeh Nahin Dekha to Kuch Nahin Dekha</h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- section start -->
			<!-- ================ -->
			<section class="pv-30">
				<div class="container">
                                    <h3 style="    color: #4c3263;">Latest <strong>Accomodation</strong></h3>
					<div class="separator-2"></div>
					<div class="row grid-space-10">
						<div class="col-sm-6 col-md-3 ">
							<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
								<div class="overlay-container">
                                                                    <img src="images/accomodation.jpg" alt="">
								</div>
								<div class="body">
                                                                    <h3 id="accomodation-title"><strong>FULL GT</strong></h3>
									<div class="separator"></div>
                                                                        <div class="body">
													<p class="small" id="accomodation-discription">Welcome Beverage ,Break fast, lunch ,Hi tea, Live BBQ Dinner (Limited),Paint ball,Pool Access, Mist dance , Eight Adventure game Coupons ( Adults only ),Use of standard facilities of the resort.</p>
                                                                                                        <p class="small" ><a id="accomodation-title">More...</a></p>
													<div class="elements-list clearfix">
                                                                                                            <span class="price" id="accomodation-title">200 /- Rs.&nbsp;&nbsp;&nbsp;</span>
														<a  href="portfolio-item.html" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Book Now<i class="fa fa-shopping-cart pl-10"></i></a>
													</div>
												</div>
									
									
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
								<div class="overlay-container">
                                                                    <img src="images/accomodation.jpg" alt="">
								</div>
								<div class="body">
                                                                    <h3 id="accomodation-title"><strong>FULL GT</strong></h3>
									<div class="separator"></div>
                                                                        <div class="body">
													<p class="small" id="accomodation-discription">Welcome Beverage ,Break fast, lunch ,Hi tea, Live BBQ Dinner (Limited),Paint ball,Pool Access, Mist dance , Eight Adventure game Coupons ( Adults only ),Use of standard facilities of the resort.</p>
                                                                                                        <p class="small" ><a id="accomodation-title">More...</a></p>
													<div class="elements-list clearfix">
                                                                                                            <span class="price" id="accomodation-title">200 /- Rs.&nbsp;&nbsp;&nbsp;</span>
														<a  href="portfolio-item.html" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Book Now<i class="fa fa-shopping-cart pl-10"></i></a>
													</div>
												</div>
									
									
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
								<div class="overlay-container">
                                                                    <img src="images/accomodation.jpg" alt="">
								</div>
								<div class="body">
                                                                    <h3 id="accomodation-title"><strong>FULL GT</strong></h3>
									<div class="separator"></div>
                                                                        <div class="body">
													<p class="small" id="accomodation-discription">Welcome Beverage ,Break fast, lunch ,Hi tea, Live BBQ Dinner (Limited),Paint ball,Pool Access, Mist dance , Eight Adventure game Coupons ( Adults only ),Use of standard facilities of the resort.</p>
                                                                                                        <p class="small" ><a id="accomodation-title">More...</a></p>
													<div class="elements-list clearfix">
                                                                                                            <span class="price" id="accomodation-title">200 /- Rs.&nbsp;&nbsp;&nbsp;</span>
														<a  href="portfolio-item.html" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Book Now<i class="fa fa-shopping-cart pl-10"></i></a>
													</div>
												</div>
									
									
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
								<div class="overlay-container">
                                                                    <img src="images/accomodation.jpg" alt="">
								</div>
								<div class="body">
                                                                    <h3 id="accomodation-title"><strong>FULL GT</strong></h3>
									<div class="separator"></div>
                                                                        <div class="body">
													<p class="small" id="accomodation-discription">Welcome Beverage ,Break fast, lunch ,Hi tea, Live BBQ Dinner (Limited),Paint ball,Pool Access, Mist dance , Eight Adventure game Coupons ( Adults only ),Use of standard facilities of the resort.</p>
                                                                                                        <p class="small" ><a id="accomodation-title">More...</a></p>
													<div class="elements-list clearfix">
                                                                                                            <span class="price" id="accomodation-title">200 /- Rs.&nbsp;&nbsp;&nbsp;</span>
														<a  href="portfolio-item.html" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Book Now<i class="fa fa-shopping-cart pl-10"></i></a>
													</div>
												</div>
									
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>
			</section>
                        <section id="dishes" class="section default-bg clearfix" style=" background-color: #42275b;  "></section>
			<!-- section end -->
<section class="pv-30">
				<div class="container">
                                    <h3 style="    color: #4c3263;">Latest <strong>Offers</strong></h3>
					<div class="separator-2"></div>
					<div class="row grid-space-10">
						<div class="col-sm-4">
									<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
										<div class="overlay-container overlay-visible">
                                                                                    <img src="images/corporate.jpg" alt="" style="    width: 374px;height: 370px;">
											<a href="#" class="overlay-link"><i class="fa fa-link"></i></a>
											<div class="overlay-bottom hidden-xs">
												<div class="text">
                                                                                                    <p class="lead margin-clear text-left"><strong>Corporate Plan</strong></p>
												</div>
											</div>
										</div>
										<div class="body padding-horizontal-clear">
											<p id="accomodation-title"  >Relish Unlimited cocktails and BBQ for 90 min ending with dancing to the music played by our Dj under the mist!.</p>
											<a class="link-dark" href="page-services.html" id="accomodation-title">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
						<div class="col-sm-4">
									<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
										<div class="overlay-container overlay-visible">
                                                                                    <img src="images/corporate.jpg" alt="" style="    width: 374px;height: 370px;">
											<a href="#" class="overlay-link"><i class="fa fa-link"></i></a>
											<div class="overlay-bottom hidden-xs">
												<div class="text">
                                                                                                    <p class="lead margin-clear text-left"><strong>Corporate Plan</strong></p>
												</div>
											</div>
										</div>
										<div class="body padding-horizontal-clear">
											<p id="accomodation-title"  >Relish Unlimited cocktails and BBQ for 90 min ending with dancing to the music played by our Dj under the mist!.</p>
											<a class="link-dark" href="page-services.html" id="accomodation-title">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
						<div class="col-sm-4">
									<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
										<div class="overlay-container overlay-visible">
                                                                                    <img src="images/corporate.jpg" alt="" style="    width: 374px;height: 370px;">
											<a href="#" class="overlay-link"><i class="fa fa-link"></i></a>
											<div class="overlay-bottom hidden-xs">
												<div class="text">
                                                                                                    <p class="lead margin-clear text-left"><strong>Corporate Plan</strong></p>
												</div>
											</div>
										</div>
										<div class="body padding-horizontal-clear">
											<p id="accomodation-title"  >Relish Unlimited cocktails and BBQ for 90 min ending with dancing to the music played by our Dj under the mist!.</p>
											<a class="link-dark" href="page-services.html" id="accomodation-title">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
					</div>
				</div>
				<br>
			</section>
                        <section id="dishes" class="section default-bg clearfix" style=" background-color: #42275b;  "></section>
                        <section class="pv-30">
				<div class="container">
                                    <h3 style="    color: #4c3263;">Movies <strong>Comming Soon</strong></h3>
					<div class="separator-2"></div>
					<div class="row grid-space-10">
						
						<div class="col-sm-6 col-md-4">
							<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
								<div class="overlay-container">
                                                                    <iframe width="350" height="300" src="https://www.youtube.com/embed/J_yb8HORges" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
								</div>
								<div class="body">
                                                                    <h3 id="accomodation-title"><strong>Secret Super Star</strong></h3>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
								<div class="overlay-container">
                                                                    <iframe width="350" height="300" src="https://www.youtube.com/embed/J_yb8HORges" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
								</div>
								<div class="body">
                                                                    <h3 id="accomodation-title"><strong>Secret Super Star</strong></h3>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
								<div class="overlay-container">
                                                                    <iframe width="350" height="300" src="https://www.youtube.com/embed/J_yb8HORges" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
								</div>
								<div class="body">
                                                                    <h3 id="accomodation-title"><strong>Secret Super Star</strong></h3>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<br>
			</section>
                        <section id="dishes" class="section default-bg clearfix" style=" background-color: #42275b;  "></section>
			<!-- section start -->
			<!-- ================ -->
			<section class="pv-40 dark-translucent-bg parallax" style="background-image:url(images/Untitled-design-1-3-1030x538.jpg);">
				<div class="container">
                                    <div class="call-to-action text-center">
								<div class="row">
									<div class="col-sm-12">
                                                                            <center><h1 class="logo-font text-center text-muted" style="color: #ecc23a">Nights</h1></center>
                                                                                <div class="separator-2 visible-lg"></div>
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-lg-3" >
                                                                                        <div class="listing-item pl-10 pr-10 mb-20">
                                                                                            <div class="overlay-container bordered overlay-visible">
                                                                                                    <img class="to-right-block" src="images/dj nights/bollywood.PNG" style="height: 250px">
                                                                                                    <div class="overlay-bottom">
                                                                                                            <div class="text">
                                                                                                                    <h3 class="title">Bollywood Nights</h3>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-lg-3" >
                                                                                        <div class="listing-item pl-10 pr-10 mb-20">
                                                                                            <div class="overlay-container bordered overlay-visible">
                                                                                                    <img class="to-right-block" src="images/dj nights/corporate-nights.PNG" style="height: 250px">
                                                                                                    <div class="overlay-bottom">
                                                                                                            <div class="text">
                                                                                                                    <h3 class="title">Corporate Nights</h3>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-lg-3" >
                                                                                        <div class="listing-item pl-10 pr-10 mb-20">
                                                                                            <div class="overlay-container bordered overlay-visible">
                                                                                                    <img class="to-right-block" src="images/dj nights/dj-mixed-genres.PNG" style="height: 250px">
                                                                                                    <div class="overlay-bottom">
                                                                                                            <div class="text">
                                                                                                                    <h3 class="title">DJS Mixed Genres</h3>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-lg-3" >
                                                                                        <div class="listing-item pl-10 pr-10 mb-20">
                                                                                            <div class="overlay-container bordered overlay-visible">
                                                                                                    <img class="to-right-block" src="images/dj nights/edm.PNG" style="height: 250px">
                                                                                                    <div class="overlay-bottom">
                                                                                                            <div class="text">
                                                                                                                    <h3 class="title">EDM</h3>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-lg-3" >
                                                                                        <div class="listing-item pl-10 pr-10 mb-20">
                                                                                            <div class="overlay-container bordered overlay-visible">
                                                                                                    <img class="to-right-block" src="images/dj nights/hiphop-nights.PNG" style="height: 250px">
                                                                                                    <div class="overlay-bottom">
                                                                                                            <div class="text">
                                                                                                                    <h3 class="title">Hip Hop Nights</h3>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-lg-3" >
                                                                                        <div class="listing-item pl-10 pr-10 mb-20">
                                                                                            <div class="overlay-container bordered overlay-visible">
                                                                                                    <img class="to-right-block" src="images/dj nights/ladies-night.PNG" style="height: 250px">
                                                                                                    <div class="overlay-bottom">
                                                                                                            <div class="text">
                                                                                                                    <h3 class="title">Ladies Nights</h3>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-lg-3" >
                                                                                        <div class="listing-item pl-10 pr-10 mb-20">
                                                                                            <div class="overlay-container bordered overlay-visible">
                                                                                                    <img class="to-right-block" src="images/dj nights/percussionist-night.PNG" style="height: 250px">
                                                                                                    <div class="overlay-bottom">
                                                                                                            <div class="text">
                                                                                                                    <h3 class="title">Percussionist Nights</h3>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-lg-3" >
                                                                                        <div class="listing-item pl-10 pr-10 mb-20">
                                                                                            <div class="overlay-container bordered overlay-visible">
                                                                                                    <img class="to-right-block" src="images/dj nights/romantic-dance.PNG" style="height: 250px">
                                                                                                    <div class="overlay-bottom">
                                                                                                            <div class="text">
                                                                                                                    <h3 class="title">Romantic Dines</h3>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                
									</div>
								</div>
							</div>
						</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			
			<!-- section end -->

			<!-- footer top start -->
			<!-- ================ -->
			
			<!-- footer top end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php';?>
			<!-- footer end -->
			
		</div>
		<?php include 'includes/jslink.php';?>
		
		
	</body>
        <script>
            $(window).resize(function (){
//                document.location="index.php";
            })
            $(document).ready(function (){
               if($(window).width()<892){
                   setReposive();
               }
            });
            function setReposive(){
                
                $("#logo_img").css({'margin-top':'-5px'});
                $("#layer_trans_icon").css({'padding-left':'0px','padding-right':'0px'});
                $(".header-top").css({'display':'none'});
                $(".asas_asdas").css({'display':'none'});
            }
        </script>
</html>