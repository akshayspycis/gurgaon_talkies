<html lang="en">
        <head>
		<meta charset="utf-8">
		<title> Fun Gaming | Gurgaon Talkies </title>
		<meta name="description" content="Many entertaining games for all the age group is available here. Bring in your family and everyone shall be entertained for sure, while you just chill and relax.">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php include 'includes/css.php';?>
                
	</head>
        <style>
            .title{
                color: #333;
                    font-family: cursive,sans-serif;
            }
            .about-us-discription{
                color: black;
                /*text-align: center;*/
            }
            .banner.dark-translucent-bg, .banner.default-translucent-bg, .banner.light-translucent-bg, .banner.parallax {
    min-height: 411px;
            }
            .dark-translucent-bg:after {
     background-color: rgba(0,0,0,.0); 
}
        </style>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
			<?php include 'includes/header.php';?>
                        <div class="banner dark-translucent-bg" style="background-image:url('images/fun-gaming.jpg'); background-position: 50% 27%;">
				<div class="container">
					<div class="row">
						<div class="col-md-8 text-center col-md-offset-2 pv-20">
						</div>
					</div>
				</div>
			</div>
                    <section class="dark-translucent-bg pv-30" style="background-image:url(images/fine-dining-bg.jpg);background-color: rgba(0,0,0,.5); ">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action">
								<div class="row">
									<div class="col-sm-8 col-md-offset-2">
										<h3 class="mt-10 text-muted title text-center">Agar Yeh Nahin Dekha to Kuch Nahin Dekha</h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
                    <section class=" pv-40">
				<div class="container">
					<div class="row about-us-discription light-gray-bg">
                                            <div class="owl-carousel content-slider-with-controls">
										<div class="overlay-container overlay-visible">
                                                                                    <div class="col-md-6"><img src="images/fun-gaming-slide-1.jpg" ></div>
                                                                                                <div class="col-md-6">
                                                                                                    <div class="testimonial text-center">
									<div class="testimonial-image">
                                                                            <img src="https://scontent.fblr6-1.fna.fbcdn.net/v/t31.0-8/23916722_1531878280212135_1840859371717502445_o.jpg?_nc_cat=0&amp;oh=a9a229ac52bc710c9741f764eebee0ce&amp;oe=5BA667A0" alt="Jane Doe" title="Jane Doe" class="img-circle" style="margin-left: 14px;">
									</div>
                                                                                                        <h3 class="title" style="    color: #42275b;">Just Perfect!</h3>
									<div class="separator"></div>
									<div class="testimonial-body">
										<blockquote>
											<p>My family favorite resort. Its amazing, i love each and everything about Club . I have been there many times, I've never felt bored of being over there.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><strong>Akshay Bilani</strong></div>
									</div>
								</div>
                                                                                                </div>
										</div>
										<div class="overlay-container overlay-visible">
                                                                                                <div class="col-md-6">
                                                                                                    <div class="testimonial text-center">
									<div class="testimonial-image">
                                                                            <img src="https://scontent.fblr6-1.fna.fbcdn.net/v/t31.0-8/23916722_1531878280212135_1840859371717502445_o.jpg?_nc_cat=0&amp;oh=a9a229ac52bc710c9741f764eebee0ce&amp;oe=5BA667A0" alt="Jane Doe" title="Jane Doe" class="img-circle" style="margin-left: 14px;">
									</div>
                                                                                                        <h3 class="title" style="    color: #42275b;">Just Perfect!</h3>
									<div class="separator"></div>
									<div class="testimonial-body">
										<blockquote>
											<p>My family favorite resort. Its amazing, i love each and everything about Club . I have been there many times, I've never felt bored of being over there.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><strong>Akshay Bilani</strong></div>
									</div>
								</div>
                                                                                                </div>
                                                                                    <div class="col-md-6"><img src="images/fun-gaming-slide-2.jpg" ></div>
										</div>
										<div class="overlay-container overlay-visible">
                                                                                    <div class="col-md-6"><img src="images/fun-gaming-slide-3.jpg" ></div>
                                                                                                <div class="col-md-6">
                                                                                                    <div class="testimonial text-center">
									<div class="testimonial-image">
                                                                            <img src="https://scontent.fblr6-1.fna.fbcdn.net/v/t31.0-8/23916722_1531878280212135_1840859371717502445_o.jpg?_nc_cat=0&amp;oh=a9a229ac52bc710c9741f764eebee0ce&amp;oe=5BA667A0" alt="Jane Doe" title="Jane Doe" class="img-circle" style="margin-left: 14px;">
									</div>
                                                                                                        <h3 class="title" style="    color: #42275b;">Just Perfect!</h3>
									<div class="separator"></div>
									<div class="testimonial-body">
										<blockquote>
											<p>My family favorite resort. Its amazing, i love each and everything about Club . I have been there many times, I've never felt bored of being over there.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><strong>Akshay Bilani</strong></div>
									</div>
								</div>
                                                                                                </div>
										</div>
										<div class="overlay-container overlay-visible">
                                                                                                <div class="col-md-6">
                                                                                                    <div class="testimonial text-center">
									<div class="testimonial-image">
                                                                            <img src="https://scontent.fblr6-1.fna.fbcdn.net/v/t31.0-8/23916722_1531878280212135_1840859371717502445_o.jpg?_nc_cat=0&amp;oh=a9a229ac52bc710c9741f764eebee0ce&amp;oe=5BA667A0" alt="Jane Doe" title="Jane Doe" class="img-circle" style="margin-left: 14px;">
									</div>
                                                                                                        <h3 class="title" style="    color: #42275b;">Just Perfect!</h3>
									<div class="separator"></div>
									<div class="testimonial-body">
										<blockquote>
											<p>My family favorite resort. Its amazing, i love each and everything about Club . I have been there many times, I've never felt bored of being over there.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><strong>Akshay Bilani</strong></div>
									</div>
								</div>
                                                                                                </div>
                                                                                    <div class="col-md-6"><img src="images/fun-gaming-slide-4.jpg" ></div>
										</div>
									</div>
                                                                
						
					</div>
				</div>
			</section>
                    
                    <section id="dishes" class="section default-bg clearfix" style=" background-color: #42275b;  "></section>
                        <section class="main-container padding-bottom-clear">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">
							<h3 class="title">Our <strong>Five Fun Beach Games for the Whole Family</strong></h3>
							<div class="separator-2"></div>
                                                        <br>
							<div class="row light-gray-bg">
                                                               <div class="col-md-12" style="color: black;" >
                                                                   <div class="col-md-6">
                                                                       <img src="images/fun-gaming-ins.jpg" alt="">
                                                                   </div>
                                                                   <div class="col-md-6">
                                                                       <ul class="list-icons">
										<li><i class="icon-check-1"></i> Bocce</li>
										<li><i class="icon-check-1"></i> Cornhole</li>
										<li><i class="icon-check-1"></i> Ladder Ball</li>
										<li><i class="icon-check-1"></i> Myrtle Ball</li>
										<li><i class="icon-check-1"></i> Volleyball</li>
										
									</ul>
                                                                   </div>
								</div>
<!--                                                               <div class="col-md-6" style="color: black;" >
                                                                   <div class="col-md-6">
                                                                       <img src="images/relaxing-poll-baby-2.jpg" alt="">
                                                                   </div>
                                                                   <div class="col-md-6">
                                                                       <ul class="list-icons">
										<li><i class="icon-check-1"></i> Best Adult Pool </li>
										<li><i class="icon-check-1"></i> Glow Adult Pool</li>
										<li><i class="icon-check-1"></i> Best Adult Floaties</li>
										<li><i class="icon-check-1"></i> Kemuse Inflatable Adult Pool </li>
										<li><i class="icon-check-1"></i> Float Swimming Ring</li>
										<li><i class="icon-check-1"></i> Swimming Tube</li>
									</ul>
                                                                   </div>
								</div>-->
                                                               
							</div>
						</div>
						<!-- main end -->

					</div>
				</div>

                        </section>
                    <p>&nbsp;</p>
                    <section id="dishes" class="section default-bg clearfix" style=" background-color: #42275b;  "></section>
                    <section class="main-container padding-bottom-clear">
				<div class="container">
					<div class="row">
						<div class="main col-md-12">
							<h3 class="title pull-right">Fun <strong>Gaming</strong></h3>
                                                        <p>&nbsp;</p>
							<div class="separator-3"></div>
                                                        <br>
							<div class="row ">
                                                            <div class="col-md-12">
									<div class="owl-carousel content-slider-with-controls">
										<div class="overlay-container overlay-visible">
                                                                                    <img src="images/fun-gaming-banner.jpg" alt="">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                        </section>
                    <p>&nbsp;</p>
                    <section id="dishes" class="section default-bg clearfix" style=" background-color: #42275b;  "></section>
                    <section class="full-width-section">
                        
                        <div class="container">
                            
                            <p>&nbsp;</p>
                            <h3 class="title"><strong>Celebrate</strong></h3>
                            <div class="separator-2"></div>
                            <div class="col-md-6">
                                <div class="plan shadow light-gray-bg bordered">
                                        <div class="fulltransparency ">
                                            <div style="padding:12px;">
                                                <img class="to-right-block" src="images/fun-gaming-celebration-1.jpg">
                                            </div>
                                        </div>
                                </div>	
                            </div>
                            <div class="col-md-6">
                                <div class="plan shadow light-gray-bg bordered">
                                        <div class="fulltransparency ">
                                            <div style="padding:12px;">
                                                <img class="to-right-block" src="images/fun-gaming-celebration-2.jpg">
                                            </div>
                                        </div>
                                </div>	
                            </div>
                            <div class="col-md-6">
                                <div class="plan shadow light-gray-bg bordered" style="background-color:#eeeeee; ">
                                        <div class="fulltransparency ">
                                            <div style="padding:12px;">
                                                <img class="to-right-block" src="images/fun-gaming-celebration-3.jpg">
                                            </div>
                                        </div>
                                </div>	
                            </div>
                            <div class="col-md-6">
                            <div class="plan shadow light-gray-bg bordered" style="background-color:#eeeeee; ">
                                    <div class="fulltransparency ">
                                        <div style="padding:12px;">
                                            <img class="to-right-block" src="images/fun-gaming-celebration-4.jpg">
                                        </div>
                                    </div>
                            </div>	
			</div>
                        </div>
                    </section>
                    <p>&nbsp;</p>
                    <section id="dishes" class="section default-bg clearfix" style=" background-color: #42275b;  "></section>
                    <section class="main-container padding-bottom-clear">

				<div class="container">
					<div class="row">
						<div class="main col-md-12">
                                                    <h3 class="title"><strong>Testimonials on </strong><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAAAaCAMAAACNbz5NAAAAyVBMVEX///8AAABJjC6MjIzU4s+9vb2goKClw5tLjTGfv5U5OTnBwcFGiyr5+/jX19fS0tL29vbf39//2IH/3oSSt4bq8ehnZ2cxMTF+fn7C1rzb5teMs3+oqKjo6OhsoFpWkz8pKSmtyKV4p2himk2ErXZxcXE+hx6zs7MPDw9YWFi4z7H63+AvgQBNTE3Qd3m1AADptbYgICBDQ0OzmmLVtnLuy30ACRJhVDaRe0qeiVl0Yjw2OT8nKzJQRjE8NirIrnX/6ozhoqMiHhU4B9JLAAAD9klEQVRIie2XbYOaRhDHdwBhkV3FE1CQJ0VA6ZlWc+klTdsk/f4fqjO7msg9aC590by4eXHszi7M72b+OyBjLzT/2gZ3nfTm8qUR+pa1893HPz9uTe9i5AjGVx6UC+t82vHuP1DZQ4A/3n94++H9PUDUC93WZxMfwL7GZfS41mL1Q0Sj0WjSAtzffZ5q+/zpb9iOya3WPSjPtw/m1wr5gCsMwh/iMuFo9++mNzc307u/To6BWh/D7GUPfMD1HZYkT2gQKwP1KMvS3+FuejN9C2WbZd4WYEirk9SEfZtO2CgdMTsaMRqPcdxGUarul7G7CGI97BaBRVyy66Re62TSqWNgLZ1YJS7sgsVSjZIulEvMZmJZyWMudjgpeQvvpndg6omtB7ZOXUpzlKDHSkyjCfaBvDMqad40jVAaCgtDNLfuCvNVNEu6O24q5jYuAla4q8nJJQTtd3DoNstK3CYsjOPwccLGGCyrYYdw+/vpP1vG0tnGpvpS1MwzofQGmQKc2xM2VFwAbYaijHDHIpbMKgw8dDnPExnzglssNip6doWIjoFcCyMIpYWsieBuKDsuMMMOLwqnw3wlyROVnw+xlBGGH7MWPsEEo6btJkKnrpOn9WVTstCOXJTiAUB2fEhsrJklCipPzJELiTCwJTBDimvFdaVxsKDLkhfExXX9wqfKiLkxd+yLbxLfbI9/Miv2kS/a9rmU3o5c+igcUcMkiXmOsVRDkJQv7Fo5dbL4yOXyQncxoVEkITm051mDlkX2b7++yYD5G9ixETB+K3cDNj/0uaIzrrkal9TLLCWwImcBd5VXn0cMrNKludjaMCp0hzqnjBG8XnmWK2VmxN78kpZ0NGcsA79zfCxUvbvOlaKOuWupfPW5lsZiRbU8RY8rLpYsNHpczgWu7RZRTGytHh4BQLHX5Xh0wKJtzOe5dCqpjrkKngiq45qcuo54QWPfuKiWQspeHS/ma4BSmhygHFCjAOoOKOta+zVX+ZhLCd7DiywEHXGSSnKue6XquMeFWxNW8UBlT+n+Ihcblr7+StC9ytPeCdSnAYx9/yHXcOKPN+TKuUMHryCZ81Uo40Lni0mjkicuucQG1XFD4sHlHfUVuusKVwYbeuNgD0sjSGswibE9Hj9GKqJ39YlrpriiDf0Le59ap1EVjVtU1Fe5MJpg0ehuFOgTSH1VFk1RCEEOV4iiEoJK7jQXuZgfAWxUpvyDytpmD0dlK+56uPOYF+nvCBM/Nkj35m6r9ZcEqyBhAYWQ7modsy44diXdw2N6SVnueu1oxVuLPF/EX1cuWpaatvcVszXNNru03Tyj/pnsletlNof6+qb/wfzs6o+PV3u1n9r+BZgqTdETdiSlAAAAAElFTkSuQmCC"/></h3>
							<div class="separator-2"></div>
                                                        <br>
							<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 about-us-discription light-gray-bg">
							<div class="testimonial text-center">
								<div class="testimonial-image">
                                                                    <img src="https://scontent.fblr6-1.fna.fbcdn.net/v/t31.0-8/23916722_1531878280212135_1840859371717502445_o.jpg?_nc_cat=0&oh=a9a229ac52bc710c9741f764eebee0ce&oe=5BA667A0" alt="Jane Doe" title="Jane Doe" class="img-circle" style="margin-left: 14px;">
								</div>
                                                            <h3 class="title">Just Perfect!</h3>
								<div class="separator"></div>
								<div class="testimonial-body">
									<blockquote>
										<p>It was fun, and lot relaxing, at the same time. Music, food and the company of friends makes for a wonderful retreat away from hustle-bustle of the city.</p>
									</blockquote>
									<div class="testimonial-info-1">Akshay Bilani</div>
									<div class="testimonial-info-2">By Infopark</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                                                        <br>
                                                        <div class="separator-3"></div>
						</div>
						<!-- main end -->

					</div>
				</div>

                        </section>
                    
                    <p>&nbsp;</p>
                    <section id="dishes" class="section default-bg clearfix" style=" background-color: #42275b;  "></section>
                    <section class="pv-30 ">
                            <div class="" >
					<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2" style="color: #fff">
                                                    <p>&nbsp;</p>
                                                    <h2 class="logo-font text-center title"><strong>Our Gallery</strong></h2>
							<div class="separator"></div>

							<br>
						</div>
					</div>
                                            
                                        </div>
                                <div class="owl-carousel carousel">
						<div class="image-box shadow text-center">
							<div class="overlay-container">
                                                            <img src="images/gallery/2 (14).jpeg" alt="">
							</div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container">
                                                            <img src="images/gallery/2 (12).jpeg" alt="">
							</div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container">
                                                            <img src="images/gallery/2 (15).jpeg" alt="">
							</div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container">
                                                            <img src="images/gallery/2 (17).jpeg" alt="">
							</div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container">
                                                            <img src="images/gallery/2 (17).jpeg" alt="">
							</div>
						</div>
				</div>
			</section>
                    
			<?php include 'includes/footer.php';?>
			<!-- footer end -->
			
		</div>
		
		<?php include 'includes/jslink.php';?>
		
		
	</body>
        <script>
            $(window).resize(function (){
//                document.location="index.php";
            })
            $(document).ready(function (){
               if($(window).width()<892){
                   setReposive();
               }
            });
            function setReposive(){
                
                $("#logo_img").css({'margin-top':'-5px'});
                $("#layer_trans_icon").css({'padding-left':'0px','padding-right':'0px'});
                $(".header-top").css({'display':'none'});
                $(".asas_asdas").css({'display':'none'});
            }
        </script>
</html>